//
//  WatchlistTabs.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

//struct WatchlistTabs: Codable {
//    let tabs: [TabsCategories]
//}

struct TabsCategories: Codable {
    let id: Int
    let title: String
    let tabs: [Tabs]
    
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case title = "Title"
        case tabs = "Tabs"
    }
}

struct Tabs: Codable {
    let tabId: Int
    let tabName: String
    let lastModified: Date
    
    private enum CodingKeys: String, CodingKey {
        case tabId = "TabId"
        case tabName = "TabName"
        case lastModified = "LastModified"
    }
}
