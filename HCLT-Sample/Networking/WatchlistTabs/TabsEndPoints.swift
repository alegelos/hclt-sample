//
//  TabsEndPoints.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

struct TabEndPoints {
    static private let tabServicePath = "https://firebasestorage.googleapis.com/v0/b/testtask01-c5746.appspot.com/o/TabList.json?alt=media&token=e99cf91c-d5ce-40b1-8632-7089c4ced96a"
    
    static var tabsURL = URL(string: TabEndPoints.tabServicePath)
}
