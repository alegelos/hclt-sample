//
//  TabsServiceErrors.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

struct TabServiceErrors {
    enum EndPointError: Error {
        case NilEndPoint
    }
    
    enum ResponseError: Error {
        case BadResponse
    }
    
    enum DataError: Error {
        case NilData
    }
}
