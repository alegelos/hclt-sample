//
//  DateFormatter+WatchlistTabsDate.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

extension DateFormatter {
  static let WatchlistTabsDate: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    return formatter
  }()
}
