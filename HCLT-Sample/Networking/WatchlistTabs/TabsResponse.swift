//
//  TabsResponse.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

struct TabsResponse<T: Codable>: Codable {
    let watchlistTabs: [T]
    
    private enum CodingKeys: String, CodingKey {
        case watchlistTabs = "WatchlistTabs"
    }
}
