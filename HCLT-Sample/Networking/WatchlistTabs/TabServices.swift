//
//  TabServices.swift
//  HCLT-Sample
//
//  Created by Alejandro Gelos on 2/4/20.
//  Copyright © 2020 Alejandro Gelos. All rights reserved.
//

import Foundation

struct TabServices {
    func tabsCategories(completion: @escaping (Result<[TabsCategories], Error>) -> Void) {
        guard let tabURL: URL = TabEndPoints.tabsURL else {
            completion(.failure(TabServiceErrors.EndPointError.NilEndPoint))
            return
        }
        let task = URLSession.shared.dataTask(with: tabURL) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200 else {
                    completion(.failure(TabServiceErrors.ResponseError.BadResponse))
                    return
            }
            guard let data = data else {
                completion(.failure(TabServiceErrors.DataError.NilData))
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(DateFormatter.WatchlistTabsDate)
                let tabsCategories = try decoder.decode(TabsResponse<TabsCategories>.self, from: data)
                completion(.success(tabsCategories.watchlistTabs))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
}
